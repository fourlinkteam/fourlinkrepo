<?php
header( "refresh:5;url=contact.html" ); 
?>
<!DOCTYPE HTML>
<html lang="">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<style>
        body{
            background-color: #f5f7fa;
        }
        .panel-heading h4{
            margin: 0px;
        }
        .panel{
            max-width: 290px;
            margin: 50px auto 0px auto;
        }
	</style>
</head>
<body>
<?php
if($_POST) {
    
    // Enter the email where you want to receive the message
    $emailTo = 'info@fourlinksts.com';

    $name 		 = trim(ucfirst(strtolower($_POST['name'])));
    $email		 = trim($_POST['email']);
    $message 	 = trim($_POST['message']);
    
    $subject	 = "Mail from fourlinks.bodhiinfo.com Contact page  ";

     // Send email
	$headers = "From: " . $name . " <" . $email . ">" . "\r\n" . "Reply-To: " . $email;
	$success = mail($emailTo, $subject, $message, $headers);
	
	if($success)
	{ 
		echo '<div class="panel panel-primary"><div class="panel-heading"><h4>Success</h4></div><div class="panel-body">Message has been sent.You will be directed back soon.</div></div>';
	}
	else
	{
        echo '<div class="panel panel-danger"><div class="panel-heading"><h4>Success</h4></div><div class="panel-body">Message could not be sent. Please try again. You will be directed back soon.</div></div>';
	}

}

?>
</body>
</html>
