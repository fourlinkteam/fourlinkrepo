/*jslint browser: true*/
/*global $, jQuery, alert*/
$(function(){
    'use strict';
	$('.nav_trigger').click(function() {
        $('.main_nav').slideToggle();
    });
    $('.banner').bxSlider({
        auto: true,
		pager: false
    });
   
});