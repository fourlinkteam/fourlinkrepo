<?php
header( "refresh:5;url=career_page.html" );
?>
<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<style>
        body{
            background-color: #f5f7fa;
        }
        .panel-heading h4{
            margin: 0px;
        }
        .panel{
            max-width: 290px;
            margin: 50px auto 0px auto;
        }
	</style>
</head>
<body>
<?php

require_once('PHPMailer/class.phpmailer.php');


if($_POST) 
{
	
	$fname		=	trim(ucfirst(strtolower($_POST['fname'])));
	$lname		=	trim(ucfirst(strtolower($_POST['lname'])));
	$gender	=	$_POST['gender'];
	$bdate		= 	trim($_POST['bdate']);
	$country	=	$_POST['country'];
	$mstatus	=	$_POST['mstatus'];
	$phone		=	trim($_POST['phone']);
	$email		=	$_POST['email'];
	
	$file		=	$_FILES["cv"]["tmp_name"];
	$filename	=	$_FILES["cv"]["name"];
	
	$emailTo = 'cv@fourlinksts.com';
	$subject= "Mail from fourlinks.bodhiinfo.com Career page ";
	$bodytext = " First Name : ".$fname."\r\n Last Name : ".$lname."\r\n Gender : ".$gender."\r\n Date of Birth : ".$bdate."\r\n Country : ".$country."\r\n Marital Status : ".$mstatus."\r\n Phone : ".$phone."\r\n Email : ".$email."\r\n";
	 
	$email = new PHPMailer();
	$email->From      = "career@fourlinktst.com";
	$email->FromName  = $fname;
	$email->Subject   = $subject;
	$email->Body      = $bodytext;
	$email->AddAddress($emailTo);

	$file_to_attach = $file;

	$email->AddAttachment( $file_to_attach , $filename );

	if($email->Send())
	{

         
        echo '<div class="panel panel-primary"><div class="panel-heading"><h4>Success</h4></div><div class="panel-body">Message has been sent.You will be directed back soon.</div></div>';
//header( "refresh:5;url=career_page.html" );
	}
	else
	{
       
		echo '<div class="panel panel-danger"><div class="panel-heading"><h4 class="text-danger">Success</h4></div><div class="panel-body">Message could not be sent. Please try again. You will be directed back soon.</div></div>';
 //header( "refresh:5;url=career_page.html" ); 
	}
}

?>
</body>
</html>